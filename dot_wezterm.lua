local wezterm = require 'wezterm'
local act = wezterm.action

local config = wezterm.config_builder and wezterm.config_builder() or {}


--------------------------------------------------------------------------------
-- Visuals
--------------------------------------------------------------------------------

config.color_scheme = "Dracula"
config.font = wezterm.font("Cascadia Code NF")
config.font_size = 14.0


--------------------------------------------------------------------------------
-- Keybinds
--------------------------------------------------------------------------------

config.keys = {
	{ key = 'v',  mods = 'ALT',       action = act.PasteFrom 'Clipboard' },
	{ key = '{',  mods = 'ALT|SHIFT', action = act.ActivateTabRelative(-1) },
	{ key = '}',  mods = 'ALT|SHIFT', action = act.ActivateTabRelative(1) },
	{ key = 't',  mods = 'ALT',       action = act.SpawnTab 'DefaultDomain' },
	{ key = 'w',  mods = 'ALT',       action = act.CloseCurrentTab({ confirm = false }) },
	{ key = 'p',  mods = 'ALT',       action = act.ShowLauncher },
	{ key = 'F9', mods = 'ALT',       action = act.ShowTabNavigator },
}

-- https://wezfurlong.org/wezterm/config/keys.html

--------------------------------------------------------------------------------
-- Mouse Bindings
--------------------------------------------------------------------------------

config.mouse_bindings = {
	{
		event = { Up = { streak = 1, button = 'Left' } },
		mods = 'SUPER',
		action = act.OpenLinkAtMouseCursor,
	}
}

--------------------------------------------------------------------------------
-- Misc
--------------------------------------------------------------------------------

config.hide_tab_bar_if_only_one_tab = true

config.default_cwd = wezterm.home_dir

config.default_prog = { os.getenv("HOME") .. "/.local/share/mise/installs/cargo-nu/latest/bin/nu" }
-- config.default_prog = { os.getenv("HOME") .. "/.local/share/mise/installs/cargo-nu/0.92.2/bin/nu" }

return config
