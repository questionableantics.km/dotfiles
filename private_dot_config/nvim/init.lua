------------------------------------------------------------------------------------------
----- Important
------------------------------------------------------------------------------------------

vim.opt.emoji = true


------------------------------------------------------------------------------------------
----- Not Important
------------------------------------------------------------------------------------------

-- Needs to be set before any plugins are loaded
vim.g.mapleader = " "
-- vim.g.maplocalleader = " "

-- built in replacement for impatient.nvim
if vim.loader then
	vim.loader.enable()
end

require("config")
require("plugins")
require("mappings")
require("autocommands")
