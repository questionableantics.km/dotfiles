local map = vim.keymap.set

map({ "i", "c" }, "jk", "<ESC>")
map("n", ";", ":")
map({ "n", "v", "o" }, "L", "$")
map({ "n", "v", "o" }, "H", "^")

-- easy movement across buffers
map("", "<C-l>", "<C-w>l")
map("", "<C-h>", "<C-w>h")
map("", "<C-j>", "<C-w>j")
map("", "<C-k>", "<C-w>k")

-- tabs
map("n", "<Leader>ta", ":tabnew<CR>", { desc = "New Tab" })
map("n", "<Leader>tn", ":tabnext<CR>", { desc = "Next Tab" })
map("n", "<Leader>tp", ":tabprevious<CR>", { desc = "Previous Tab" })
map("n", "<Leader>tc", ":tabclose<CR>", { desc = "Close Tab" })

-- lsp
map("n", "gd", vim.lsp.buf.definition, { desc = "Go to Definition" })
map("n", "gr", vim.lsp.buf.rename, { desc = "Rename" })
map("n", "gs", vim.lsp.buf.signature_help, { desc = "Signature Help" })
map("n", "K", vim.lsp.buf.hover, { desc = "Signature Help" })
map("n", "<Leader>ca", vim.lsp.buf.code_action, { desc = "Code Actions" })
map("n", "[e", vim.diagnostic.goto_prev, { desc = "Previous Diagnostic" })
map("n", "]e", vim.diagnostic.goto_next, { desc = "Next Diagnostic" })

-- misc
map("n", "<ESC>", "<ESC>:noh<CR>", { desc = "Remove Highlights" })
map("n", "_", ":vertical resize -10<CR>", { desc = "Vertical Size Decrease" })
map("n", "+", ":vertical resize +10<CR>", { desc = "Vertical Size Increase" })
map("n", "=", ":resize +5<CR>", { desc = "Horizontal Size Increase" })
map("n", "-", ":resize -5<CR>", { desc = "Horizontal Size Decrease" })
map("n", "j", "gj", { desc = "Move Down" })
map("n", "k", "gk", { desc = "Move Up" })

local function get_cwd_path()
	local path = vim.fn.expand("%")
	local cwd = vim.fn.getcwd()

	return path:gsub(cwd, "")
end

local ts_utils = require("nvim-treesitter.ts_utils")

local M = {}

function M.get_current_function_name()
	local current_node = ts_utils.get_node_at_cursor()

	if not current_node then
		return ""
	end

	local expr = current_node

	while expr do
		if expr:type() == "function_definition" then
			break
		end
		expr = expr:parent()
	end

	if not expr then
		return ""
	end

	print(expr:child(1))

	-- return (vim.treesitter.query.get_node_text(expr:child(1)))[1]
	return (ts_utils.get_node_text(expr:child(1)))[1]
end

map("n", "cp", function()
	vim.fn.setreg("+", get_cwd_path())
end, { desc = "Copy CWD path to clipboard" })

map("n", "crp", function()
	vim.fn.setreg("+", vim.fn.expand("%:~"))
end, { desc = "Copy relative path to clipboard" })

map("n", "cn", function()
	vim.fn.setreg("+", vim.fn.expand("%:t"))
end, { desc = "Copy file name to clipboard" })

map("n", "cln", function()
	vim.fn.setreg("+", vim.fn.line("."))
end, { desc = "Copy line number to clipboard" })

map("n", "cpt", function()
	local cwd_path = get_cwd_path()
	local function_name = M.get_current_function_name()

	vim.fn.setreg("+", cwd_path .. "::" .. function_name)
end, { desc = "Copy current file path and python function name" })

local run_command_in_tmux_split = function(command)
	local tmux_command = [[ tmux set-option remain-on-exit on;]]
	local cwd = vim.loop.cwd()
	tmux_command = tmux_command .. "tmux split-window -c " .. vim.fn.shellescape(cwd) .. " -v "
	tmux_command = tmux_command .. vim.o.shell .. " -c "
	tmux_command = tmux_command .. "'" .. command .. "'"

	vim.fn.jobstart(tmux_command)
end

-- map("n", "<Leader>tn", function()
-- 	local cwd_path = get_cwd_path()
-- 	local function_name = M.get_current_function_name()
-- 	local cwd = vim.loop.cwd()
--
-- 	vim.schedule(function()
-- 		local pytest_command = "poetry -C "
-- 			.. cwd
-- 			.. " "
-- 			.. "run pytest "
-- 			.. cwd_path
-- 			.. "::"
-- 			.. function_name
-- 			.. ";"
--
-- 		run_command_in_tmux_split(pytest_command)
-- 	end)
-- end, { desc = "Run nearest python test in tmux split terminal" })

-- additional undo breakpoints
map("i", ",", ",<C-g>U")
map("i", ".", ".<C-g>U")
map("i", "!", "!<C-g>U")
map("i", "?", "?<C-g>U")

vim.cmd(
	[[
  	:abbreviate waj wa
	]],
	false
)
