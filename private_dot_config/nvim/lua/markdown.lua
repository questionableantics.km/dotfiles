local M = {}

function string.starts(String, Start)
	return string.sub(String, 1, string.len(Start)) == Start
end

M.is_checkbox = function()
	-- Get the current line
	local current_line = vim.fn.getline(".")
	if
		string.starts(current_line, "- [ ]")
		or string.starts(current_line, "- [x]")
		or string.starts(current_line, "* [ ]")
		or string.starts(current_line, "* [x]")
	then
		return true
	else
		return false
	end
end

M.is_bullet = function()
	-- Get the current line
	local current_line = vim.fn.getline(".")
	if string.starts(current_line, "- ") or string.starts(current_line, "* ") then
		return true
	else
		return false
	end
end

M.is_checkbox_checked = function()
	-- Get the current line
	local current_line = vim.fn.getline(".")

	return string.starts(current_line, "- [x]") or string.starts(current_line, "* [x]")
end

M.toggle_checkbox = function()
	-- Get the current line
	local current_line = vim.fn.getline(".")
	-- Get the current line number
	local line_number = vim.fn.line(".")

	if M.is_checkbox() then
		if M.is_checkbox_checked() then
			-- uncheck the box
			local new_line = "- [ ]" .. string.sub(current_line, 6)
			-- Set the modified line
			vim.fn.setline(line_number, new_line)
		else
			-- check the box
			local new_line = "- [x]" .. string.sub(current_line, 6)
			-- Set the modified line
			vim.fn.setline(line_number, new_line)
		end
	elseif M.is_bullet() then
		-- convert the bullet to a checkbox
		local new_line = "- [ ] " .. string.sub(current_line, 3)
		-- Set the modified line
		vim.fn.setline(line_number, new_line)
	else
		local new_line = "- [ ] " .. current_line
		vim.fn.setline(line_number, new_line)
		-- move to end of line
		vim.fn.cursor(line_number, #new_line + 1)
	end
end

M.continue_bulleted_list = function(opts)
	opts = opts or {}
	local forward = opts.forward or true

	-- Get the current line
	local current_line = vim.fn.getline(".")
	-- Get the current line number
	local line_number = vim.fn.line(".")

	if string.starts(current_line, "- ") or string.starts(current_line, "* ") then
		local indent = string.match(current_line, "^%s*")

		local new_line = M.is_checkbox() and indent .. "- [ ] " or indent .. "- "

		print(forward, line_number, new_line)

		-- Insert the new line
		if forward then
			vim.fn.append(line_number, new_line)
			-- Move the cursor to the new line
			vim.fn.cursor(line_number + 1, #new_line + 1)
		else
			vim.fn.append(line_number - 1, new_line)
			-- Move the cursor to the new line
			vim.fn.cursor(line_number - 1, #new_line + 1)
		end
	elseif forward then
		-- insert a new line as normal
		vim.fn.append(line_number, "")
	else
		-- insert a new line before the current line
		vim.fn.append(line_number - 1, "")
	end

	vim.cmd("startinsert!")
end

return M
