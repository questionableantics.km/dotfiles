local M = {}

function string.starts(String,Start)
   return string.sub(String,1,string.len(Start))==Start
end

M.toggle_checkbox = function()
	-- Get the current line
	local current_line = vim.fn.getline(".")
	-- Get the current line number
	local line_number = vim.fn.line(".")

	print(current_line)
	if string.starts(current_line, "- [ ]") then
		-- check the box
		local new_line = "- [x]" .. string.sub(current_line, 6)
		-- Set the modified line
		vim.fn.setline(line_number, new_line)
	elseif string.starts(current_line, "- [x]") then
		-- uncheck the box
		local new_line = "- [ ]" .. string.sub(current_line, 6)
		-- Set the modified line
		vim.fn.setline(line_number, new_line)
	else
		print("Checkbox not detected")
		local new_line = "- [ ] " .. current_line
		vim.fn.setline(line_number, new_line)
	end
end

return M
