-- local extras = require("extras")

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	{
		-- Interact with file system like a buffer
		"stevearc/oil.nvim",
		opts = {
			view_options = { show_hidden = true },
			experimental_watch_for_changes = true,
		},
		keys = {
			{
				"<Leader>v",
				function()
					require("oil").open()
				end,
				desc = "Open File Explorer",
			},
		},
		dependencies = { "nvim-tree/nvim-web-devicons" },
	},

	{
		"ruifm/gitlinker.nvim",
		dependencies = "nvim-lua/plenary.nvim",
		config = function()
			local gitlinker = require("gitlinker")

			gitlinker.setup({
				callbacks = {
					["gitlab.internal.equipmentshare.com"] = gitlinker.hosts.get_gitlab_type_url,
				},
			})
		end,
		keys = function()
			local gitlinker = require("gitlinker")

			return {
				{
					"<Leader>gb",
					function()
						gitlinker.get_buf_range_url("n", {
							action_callback = require("gitlinker.actions").open_in_browser,
						})
					end,
					mode = { "n", "v" },
					desc = "Get Buffer Range URL",
				},
				{
					"<Leader>gB",
					function()
						gitlinker.get_repo_url({ action_callback = require("gitlinker.actions").open_in_browser })
					end,
					mode = "n",
					desc = "Get Repo URL",
				},
				{
					"<Leader>gY",
					function()
						gitlinker.get_repo_url()
					end,
					mode = "n",
					desc = "Get Repo URL",
				},
			}
		end,
	},

	{
		"ibhagwan/fzf-lua",
		opts = {
			"telescope",
			defaults = {
				formatter = "path.filename_first",
				multiline = 1,
			},
		},
		keys = function()
			local fzf = require("fzf-lua")

			local get_input = function(input_prompt)
				local success, result = pcall(function()
					return '"' .. vim.fn.input(input_prompt) .. '"'
				end)

				return success and result or nil
			end

			local gitlinker_function = function()
				local gitlinker_actions = {
					["Open in browser"] = require("gitlinker.actions").open_in_browser,
					["Copy URL"] = require("gitlinker").get_repo_url,
				}

				fzf.fzf_exec(function(fzf_fb)
					for name, _ in pairs(gitlinker_actions) do
						fzf_fb(name)
					end
				end, {
					prompt = "Gitlinker )",
					actions = {
						["default"] = function(selected)
							gitlinker_actions[selected[1]](selected)
						end,
					},
				})
			end

			local tmux = function()
				local sessions = vim.fn.systemlist("tmux list-sessions -F '#S'")

				for i = #sessions, 1, -1 do
					if string.find(sessions[i], "popup") then
						table.remove(sessions, i)
					end
				end

				local tmux_actions = {}

				for _, session in ipairs(sessions) do
					tmux_actions[session] = function()
						vim.fn.system('tmux switch -t "' .. session .. '"')
					end
				end

				fzf.fzf_exec(function(fzf_fb)
					for name, _ in pairs(tmux_actions) do
						fzf_fb(name)
					end
				end, {
					prompt = "Tmux ) ",
					actions = {
						["default"] = function(selected)
							tmux_actions[selected[1]](selected)
						end,
						["ctrl-d"] = function(selected)
							vim.fn.system("tmux kill-session -t " .. selected[1])
						end,
						["ctrl-n"] = function()
							local name = get_input("Session Name: ")

							if name ~= nil then
								vim.fn.system("tmux new -d -s " .. name)
								vim.fn.system("tmux switch -t " .. name)
							end
						end,
						["ctrl-r"] = function(selected)
							local new_name = get_input("New Name: ")

							if new_name ~= "" then
								vim.fn.system("tmux rename-session -t " .. selected[1] .. " " .. new_name)
							end
						end,
					},
				})
			end

			return {
				{
					"<Leader>ff",
					fzf.files,
					desc = "Search by Filename",
				},
				{
					"<Leader>fg",
					fzf.live_grep_glob,
					desc = "Live Grep",
				},
				{
					"<Leader>fo",
					fzf.oldfiles,
					desc = "Search Old Files",
				},
				{
					"<Leader>fl",
					fzf.resume,
					desc = "Last Search",
				},
				{
					"<Leader>fb",
					fzf.buffers,
					desc = "Search Buffers",
				},
				{
					"gh",
					fzf.lsp_references,
					desc = "Get References",
				},
				{
					"<Leader>fG",
					gitlinker_function,
					desc = "Gitlinker",
				},
				{
					"<Leader>fd",
					tmux,
					desc = "Tmux",
				},
			}
		end,
		cmd = { "FzfLua" },
		dependencies = { "nvim-tree/nvim-web-devicons" },
	},

	{
		-- Session Management
		name = "judge.nvim",
		dir = "~/.config/nvim/lua/custom-plugins/judge",
		dev = true,
		config = function()
			require("custom-plugins.judge").setup({
				ignored_directories = {
					vim.fn.expand("$HOME"),
				},
				ignored_buffer_patterns = {
					"oil://",
				},
			})
		end,
		lazy = false,
	},

	{
		"jmederosalvarado/roslyn.nvim",
		opts = {
			on_attach = function() end,
			settings = {
				["csharp|inlay_hints"] = {
					["csharp_enable_inlay_hints_for_implicit_object_creation"] = true,
					["csharp_enable_inlay_hints_for_implicit_variable_types"] = true,
					["csharp_enable_inlay_hints_for_lambda_parameter_types"] = true,
					["csharp_enable_inlay_hints_for_types"] = true,
					["dotnet_enable_inlay_hints_for_indexer_parameters"] = true,
					["dotnet_enable_inlay_hints_for_literal_parameters"] = true,
					["dotnet_enable_inlay_hints_for_object_creation_parameters"] = true,
					["dotnet_enable_inlay_hints_for_other_parameters"] = true,
					["dotnet_enable_inlay_hints_for_parameters"] = true,
					["dotnet_suppress_inlay_hints_for_parameters_that_differ_only_by_suffix"] = true,
					["dotnet_suppress_inlay_hints_for_parameters_that_match_argument_name"] = true,
					["dotnet_suppress_inlay_hints_for_parameters_that_match_method_intent"] = true,
				},
			},
		},
		ft = { "cs" },
	},

	{
		"neovim/nvim-lspconfig",
		config = function()
			local lspconfig = require("lspconfig")
			local lua_rtp = vim.split(package.path, ";")

			-- An example nvim-lspconfig capabilities setting
			local capabilities =
				require("cmp_nvim_lsp").default_capabilities(vim.lsp.protocol.make_client_capabilities())

			-- Ensure that dynamicRegistration is enabled! This allows the LS to take into account actions like the
			-- Create Unresolved File code action, resolving completions for unindexed code blocks, ...
			capabilities.workspace = {
				didChangeWatchedFiles = {
					dynamicRegistration = true,
				},
			}

			lspconfig.markdown_oxide.setup({
				capabilities = capabilities, -- again, ensure that capabilities.workspace.didChangeWatchedFiles.dynamicRegistration = true
			})

			table.insert(lua_rtp, "lua/?.lua")
			table.insert(lua_rtp, "lua/?/init.lua")

			lspconfig.basedpyright.setup({
				settings = {
					basedpyright = {
						typeCheckingMode = "basic",
					},
				},
			})

			lspconfig.lua_ls.setup({
				settings = {
					Lua = {
						hint = { enable = true },
						codelens = { enable = true },
						runtime = {
							version = "LuaJIT",
							path = lua_rtp,
						},
						diagnostics = { globals = { "vim", "coq" } },
						workspace = {
							library = vim.api.nvim_get_runtime_file("", true),
						},
						telemetry = {
							enable = false,
						},
					},
				},
			})

			local schemastore = require("schemastore")

			lspconfig.yamlls.setup({
				settings = {
					yaml = {
						validate = true,
						schemaStore = {
							enable = false,
							url = "",
						},
						schemas = schemastore.yaml.schemas(),
					},
				},
			})

			lspconfig.jsonls.setup({
				settings = {
					json = {
						schemas = schemastore.json.schemas(),
						validate = { enable = true },
					},
				},
			})

			lspconfig.nil_ls.setup{}
		end,
	},

	{
		-- direct integration with tsserver
		"pmizio/typescript-tools.nvim",
		opts = {
			tsserver_plugins = {
				{ name = "typescript-tslint-plugin" },
			},
			settings = {
				-- codelens is slow as hell atm
				-- code_lens = "references_only",
				tsserver_file_preferences = {
					includeInlayParameterNameHints = "all",
					includeInlayEnumMemberValueHints = true,
					includeInlayFunctionLikeReturnTypeHints = true,
					includeInlayVariableTypeHints = true,
				},
			},
		},
		dependencies = {
			"nvim-lua/plenary.nvim",
			"neovim/nvim-lspconfig",
		},
		ft = { "typescript", "typescriptreact", "javascript", "javascriptreact" },
	},

	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
		config = function()
			require("nvim-treesitter.configs").setup({
				modules = {},
				auto_install = true,
				ignore_install = {},
				-- either 'all' or {'a', 'list', 'of', 'languages'}
				ensure_installed = {
					"python",
					"javascript",
					"typescript",
					"c_sharp",
					"tsx",
					"lua",
					"yaml",
					"graphql",
					"java",
					"scss",
					"css",
					"html",
					"jsdoc",
					"dockerfile",
					"toml",
					"json",
					"json5",
					"markdown",
					"http",
					"bash",
					"rust",
					"go",
					"latex",
					"bibtex",
				},
				highlight = { enable = true },
				-- async installation of parsers
				sync_install = false,
				indent = { enable = true },
				textobjects = {
					select = {
						enable = true,
						-- Automatically jump forward to textobj, similar to targets.vim
						lookahead = true,
						keymaps = {
							["af"] = "@function.outer",
							["if"] = "@function.inner",
							["ac"] = "@class.outer",
							["ic"] = "@class.inner",
							["ab"] = "@block.outer",
							["ib"] = "@block.inner",
							["al"] = "@loop.outer",
							["il"] = "@loop.inner",
							["ai"] = "@conditional.outer",
							["ii"] = "@conditional.inner",
							["as"] = "@statement.outer",
							["is"] = "@statement.inner",
							["ad"] = "@comment.outer",
							["am"] = "@call.outer",
							["im"] = "@call.inner",
						},
					},
				},
			})
		end,
	},

	{
		"Mofiqul/vscode.nvim",
		config = function()
			require("vscode").setup({
				transparent = true,
				italic_comments = true,
				underline_links = true,
			})

			vim.cmd([[colorscheme vscode]])
		end,
	},

	{
		-- AI in my code
		"zbirenbaum/copilot.lua",
		opts = {
			suggestion = {
				enabled = true,
				auto_trigger = true,
				keymap = {
					accept = "<C-J>",
					accept_word = "<C-f>",
					accept_line = "<C-l>",
				},
			},
		},
		event = { "InsertEnter" },
	},

	{
		"folke/ts-comments.nvim",
		opts = {},
		event = "VeryLazy",
		enabled = vim.fn.has("nvim-0.10.0") == 1,
	},

	{
		"hrsh7th/nvim-cmp",
		config = function()
			local cmp = require("cmp")
			local autocomplete_group = vim.api.nvim_create_augroup("vimrc_autocompletion", { clear = true })

			vim.api.nvim_create_autocmd("FileType", {
				pattern = { "sql", "mysql", "plsql" },
				callback = function()
					cmp.setup.buffer({ sources = { { name = "vim-dadbod-completion" } } })
				end,
				group = autocomplete_group,
			})

			cmp.setup({
				snippet = {
					expand = function(args)
						vim.snippet.expand(args.body)
					end,
				},
				window = {
					completion = cmp.config.window.bordered(),
					documentation = cmp.config.window.bordered(),
					auto_change = true,
				},
				mapping = cmp.mapping.preset.insert({
					["<C-u>"] = cmp.mapping.scroll_docs(-4),
					["<C-d>"] = cmp.mapping.scroll_docs(4),
					["<C-Space>"] = cmp.mapping.complete(),
					["<C-e>"] = cmp.mapping.close(),
					["<CR>"] = cmp.mapping.confirm({
						behavior = cmp.ConfirmBehavior.Insert,
						select = false,
					}),
					["<C-n>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
					["<C-p>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
				}),
				sources = {
					{ name = "path" },
					{ name = "buffer" },
					{ name = "vim-dadbod-completion" },
					{
						name = "nvim_lsp",
						option = {
							markdown_oxide = {
								keyword_pattern = [[\(\k\| \|\/\|#\)\+]],
							},
						},
					},
				},
				formatting = {
					format = require("lspkind").cmp_format({
						mode = "symbol_text",
						maxwidth = 50,
						ellipsis_char = "...",
						show_labelDetails = true,
						before = function(_, vim_item)
							return vim_item
						end,
					}),
				},
				sorting = {
					comparators = {
						cmp.config.compare.kind,
						cmp.config.compare.locality,
						cmp.config.compare.recently_used,
						cmp.config.compare.score, -- based on :  score = score + ((#sources - (source_index - 1)) * sorting.priority_weight)
						cmp.config.compare.offset,
						cmp.config.compare.order,
					},
				},
			})
		end,
		dependencies = {
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-path",
			"hrsh7th/cmp-buffer",
			"onsails/lspkind-nvim",
		},
		event = "InsertEnter",
	},

	{
		-- Better Quickfix
		"kevinhwang91/nvim-bqf",
		ft = { "qf" },
	},

	{
		"kristijanhusak/vim-dadbod-ui",
		dependencies = {
			-- DB interface
			"tpope/vim-dadbod",
			-- Autocomplete source for vim dadbod
			"kristijanhusak/vim-dadbod-completion",
			-- Postgres driver
			"jackc/pgx",
		},
		cmd = {
			"DBUI",
			"DBUIToggle",
			"DBUIAddConnection",
			"DBUIFindBuffer",
		},
		init = function()
			vim.g.db_ui_use_nerd_fonts = 1
		end,
	},

	{
		-- Little bit of everything
		"echasnovski/mini.nvim",
		config = function()
			-- underline instances of the word under the cursor
			require("mini.cursorword").setup({})
			-- auto pairing of parentheses, brackets, etc.
			require("mini.pairs").setup({})
			-- additional targets to jump to next/prev
			require("mini.bracketed").setup({})
			-- additional around/in functionality
			require("mini.ai").setup({})

			local clue = require("mini.clue")

			clue.setup({
				triggers = {
					-- Leader triggers
					{ mode = "n", keys = "<Leader>" },
					{ mode = "x", keys = "<Leader>" },

					{ mode = "n", keys = "]" },
					{ mode = "n", keys = "[" },

					-- Built-in completion
					{ mode = "i", keys = "<C-x>" },

					-- Window commands
					{ mode = "n", keys = "<C-w>" },

					-- chatgpt
					{ mode = "n", keys = "<C-g>" },
					{ mode = "i", keys = "<C-g>" },
					{ mode = "v", keys = "<C-g>" },

					-- `z` key
					{ mode = "n", keys = "z" },
					{ mode = "x", keys = "z" },

					{ mode = "n", keys = "[" },
					{ mode = "n", keys = "]" },
				},
				clues = {
					clue.gen_clues.builtin_completion(),
					clue.gen_clues.g(),
					clue.gen_clues.z(),
					clue.gen_clues.windows(),
					clue.gen_clues.registers(),
				},
				window = {
					delay = 300,
				},
			})

			require("mini.surround").setup({
				n_lines = 100,
				mappings = {
					replace = "cs",
					delete = "ds",
					add = "rs",
					find = "",
					find_left = "",
					highlight = "",
					update_n_lines = "",
				},
			})
		end,
		event = "VeryLazy",
	},

	{
		-- Formatter plugin
		"stevearc/conform.nvim",
		opts = {
			formatters_by_ft = {
				csharp = { "csharpier" },
				json = { "fixjson" },
				lua = { "stylua" },
				javascript = { "prettierd" },
				javascriptreact = { "prettierd" },
				python = { "ruff_format" },
				typescript = { "prettierd" },
				typescriptreact = { "prettierd" },
				sql = { "sqlformat" },
				css = { "prettierd" },
			},
		},
		keys = {
			{
				"<leader>fq",
				function()
					print("Formatting buffer")
					require("conform").format({ bufnr = 0, lsp_fallback = true })
				end,
				desc = "Format buffer",
			},
			{
				"<leader>fq",
				function()
					print("Formatting file")
					require("conform").format({ bufnr = 0, lsp_fallback = true, range = {} })
				end,
				mode = "v",
				desc = "Format file",
			},
		},
	},

	{
		-- Additional treesitter functionality (in/around function/class/etc. operations)
		"nvim-treesitter/nvim-treesitter-textobjects",
		dependencies = {
			"nvim-treesitter/nvim-treesitter",
		},
		-- required to load before treesitter
		lazy = false,
	},

	{
		-- git in vim (required for other git plugins)
		"tpope/vim-fugitive",
		event = "VeryLazy",
	},

	{
		-- sign column symbols for git changes and git hunk actions
		"lewis6991/gitsigns.nvim",
		opts = {
			current_line_blame = true,
			current_line_blame_opts = { delay = 200 },
		},
		keys = function()
			return {
				{
					"]c",
					function()
						require("gitsigns").next_hunk()
					end,
					desc = "Next hunk",
				},
				{
					"[c",
					function()
						require("gitsigns").prev_hunk()
					end,
					desc = "Previous hunk",
				},
				{
					"<Leader>hp",
					function()
						require("gitsigns").preview_hunk_inline()
					end,
					desc = "Preview hunk",
				},
			}
		end,
		event = "VeryLazy",
	},

	{
		-- Virtual text to add indentation guides
		"shellRaining/hlchunk.nvim",
		opts = {
			indent = { chars = { "┆" } },
			blank = { enable = false },
		},
		event = { "VeryLazy" },
	},

	{
		"robitx/gp.nvim",
		opts = {
			providers = {
				anthropic = {
					endpoint = "https://api.anthropic.com/v1/messages",
					secret = "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",
				},
			},
			agents = {
				{
					provider = "anthropic",
					name = "Claude",
					chat = true,
					command = true,
					model = { model = "claude-3-5-sonnet-20240620", temperature = 0.8, top_p = 1 },
					system_prompt = [[
					You are an expert software engineer, speaking to another software engineer.

					- If you're unsure do not guess and say you don't know instead.
					- Ask questions if you need clarification to provide better answer.
					- Think deeply and carefully from first principles step by step.
					- Zoom out first to see the big picture and then zoom in to details.
					- Use Socratic method to improve your thinking and coding skills.
					- Don't elide any code from your output if the answer requires coding.
					- Take a deep breath; You've got this!
					]],
				},
				-- {
				-- 	name = "ChatGPT4",
				-- 	-- chat = true,
				-- 	-- command = true,
				-- 	-- string with model name or table with model name and parameters
				-- 	model = { model = "gpt-4o", temperature = 1.1, top_p = 1 },
				-- 	-- system prompt (use this to specify the persona/role of the AI)
				-- 	system_prompt = [[
				-- 		You are an expert software engineer, speaking to another software engineer.
				--
				-- 		- If you're unsure do not guess and say you don't know instead.
				-- 		- Ask questions if you need clarification to provide better answer.
				-- 		- Think deeply and carefully from first principles step by step.
				-- 		- Zoom out first to see the big picture and then zoom in to details.
				-- 		- Use Socratic method to improve your thinking and coding skills.
				-- 		- Don't elide any code from your output if the answer requires coding.
				-- 		- Take a deep breath; You've got this!
				-- 		]],
				-- },
			},
		},
		keys = function()
			local function makeMaps(configs)
				local maps = {}

				for _, config in ipairs(configs) do
					table.insert(maps, {
						config.lhs,
						config.rhs,
						mode = config.mode,
						desc = "GPT prompt " .. config.desc,
						noremap = true,
						silent = true,
						nowait = true,
					})
				end

				return maps
			end

			return makeMaps({
				{
					lhs = "<C-g>c",
					rhs = "<cmd>GpChatNew<cr>",
					mode = { "n", "i" },
					desc = "New Chat",
				},
				{
					lhs = "<C-g>t",
					rhs = "<cmd>GpChatToggle<cr>",
					mode = { "n", "i" },
					desc = "Toggle Chat",
				},
				{
					lhs = "<C-g>f",
					rhs = "<cmd>GpChatFinder<cr>",
					mode = { "n", "i" },
					desc = "Chat Finder",
				},
				{
					lhs = "<C-g>c",
					rhs = ":<C-u>'<,'>GpChatNew<cr>",
					mode = "v",
					desc = "Visual Chat New",
				},
				{
					lhs = "<C-g>p",
					rhs = ":<C-u>'<,'>GpChatPaste<cr>",
					mode = "v",
					desc = "Visual Chat Paste",
				},
				{
					lhs = "<C-g>t",
					rhs = ":<C-u>'<,'>GpChatToggle<cr>",
					mode = "v",
					desc = "Visual Toggle Chat",
				},
				{
					lhs = "<C-g><C-x>",
					rhs = "<cmd>GpChatNew split<cr>",
					mode = { "n", "i" },
					desc = "New Chat split",
				},
				{
					lhs = "<C-g><C-v>",
					rhs = "<cmd>GpChatNew vsplit<cr>",
					mode = { "n", "i" },
					desc = "New Chat vsplit",
				},
				{
					lhs = "<C-g><C-t>",
					rhs = "<cmd>GpChatNew tabnew<cr>",
					mode = { "n", "i" },
					desc = "New Chat tabnew",
				},
				{
					lhs = "<C-g><C-x>",
					rhs = ":<C-u>'<,'>GpChatNew split<cr>",
					mode = "v",
					desc = "Visual Chat New split",
				},
				{
					lhs = "<C-g>|>",
					rhs = ":<C-u>'<,'>GpChatNew vsplit<cr>",
					mode = "v",
					desc = "Visual Chat New vsplit",
				},
				{
					lhs = "<C-g><C-t>",
					rhs = ":<C-u>'<,'>GpChatNew tabnew<cr>",
					mode = "v",
					desc = "Visual Chat New tabnew",
				},
				{
					lhs = "<C-g>gp",
					rhs = "<cmd>GpPopup<cr>",
					mode = { "n", "i" },
					desc = "Popup",
				},
				{
					lhs = "<C-g>ge",
					rhs = "<cmd>GpEnew<cr>",
					mode = { "n", "i" },
					desc = "GpEnew",
				},
				{
					lhs = "<C-g>gn",
					rhs = "<cmd>GpNew<cr>",
					mode = { "n", "i" },
					desc = "GpNew",
				},
				{
					lhs = "<C-g>gv",
					rhs = "<cmd>GpVnew<cr>",
					mode = { "n", "i" },
					desc = "GpVnew",
				},
				{
					lhs = "<C-g>gt",
					rhs = "<cmd>GpTabnew<cr>",
					mode = { "n", "i" },
					desc = "GpTabnew",
				},

				{
					lhs = "<C-g>gp",
					rhs = ":<C-u>'<,'>GpPopup<cr>",
					mode = "v",
					desc = "Visual Popup",
				},
				{
					lhs = "<C-g>ge",
					rhs = ":<C-u>'<,'>GpEnew<cr>",
					mode = "v",
					desc = "Visual GpEnew",
				},
				{
					lhs = "<C-g>gn",
					rhs = ":<C-u>'<,'>GpNew<cr>",
					mode = "v",
					desc = "Visual GpNew",
				},
				{
					lhs = "<C-g>gv",
					rhs = ":<C-u>'<,'>GpVnew<cr>",
					mode = "v",
					desc = "Visual GpVnew",
				},
				{
					lhs = "<C-g>gt",
					rhs = ":<C-u>'<,'>GpTabnew<cr>",
					mode = "v",
					desc = "Visual GpTabnew",
				},
				{
					lhs = "<C-g>x",
					rhs = "<cmd>GpContext<cr>",
					mode = { "n", "i" },
					desc = "Toggle Context",
				},
				{
					lhs = "<C-g>x",
					rhs = ":<C-u>'<,'>GpContext<cr>",
					mode = "v",
					desc = "Visual Toggle Context",
				},
				{
					lhs = "<C-g>s",
					rhs = "<cmd>GpStop<cr>",
					mode = { "n", "i", "v", "x" },
					desc = "Stop",
				},
				{
					lhs = "<C-g>n",
					rhs = "<cmd>GpNextAgent<cr>",
					mode = { "n", "i", "v", "x" },
					desc = "Next Agent",
				},
			})
		end,
	},

	{
		"LhKipp/nvim-nu",
		opts = { use_lsp_features = false },
		ft = { "nu" },
	},
	{
		"nvim-lualine/lualine.nvim",
		opts = { options = { theme = "onedark" } },
		dependencies = { "nvim-tree/nvim-web-devicons" },
		event = "VeryLazy",
	},

	-- {
	-- 	"pogyomo/submode.nvim",
	-- 	config = function()
	-- 		local submode = require("submode")
	--
	-- 		local function create_submodes(sms)
	-- 			for _, sm in ipairs(sms) do
	-- 				submode.create(sm.name, sm.opts, unpack(sm.mappings))
	-- 			end
	-- 		end
	--
	-- 		local gitsigns = require("gitsigns")
	-- 		local gitlinker = require("gitlinker")
	--
	-- 		local submodes = {
	-- 			{
	-- 				name = "Resize",
	-- 				opts = {
	-- 					mode = "n",
	-- 					enter = { "<Leader>mr", "<C-m>r" },
	-- 					leave = { "<Esc>", "<C-c>", "q" },
	-- 				},
	-- 				mappings = {
	-- 					{
	-- 						lhs = "h",
	-- 						rhs = ":vertical resize +10<CR>",
	-- 					},
	-- 					{
	-- 						lhs = "l",
	-- 						rhs = ":vertical resize -10<CR>",
	-- 					},
	-- 					{
	-- 						lhs = "j",
	-- 						rhs = ":resize +5<CR>",
	-- 					},
	-- 					{
	-- 						lhs = "k",
	-- 						rhs = ":resize -5<CR>",
	-- 					},
	-- 				},
	-- 			},
	--
	-- 			{
	-- 				name = "Git",
	-- 				opts = {
	-- 					mode = "n",
	-- 					enter = { "<Leader>mg", "<C-m>g" },
	-- 					leave = { "<Esc>", "<C-c>", "q" },
	-- 				},
	-- 				mappings = {
	-- 					{
	-- 						lhs = "n",
	-- 						rhs = gitsigns.next_hunk
	-- 					},
	-- 					{
	-- 						lhs = "p",
	-- 						rhs = gitsigns.prev_hunk
	-- 					},
	-- 					{
	-- 						lhs = "h",
	-- 						rhs = gitsigns.preview_hunk_inline
	-- 					},
	-- 					{
	-- 						lhs = "B",
	-- 						rhs = function()
	-- 							gitlinker.get_repo_url({
	-- 								action_callback = require("gitlinker.actions")
	-- 									.open_in_browser
	-- 							})
	-- 						end,
	-- 					}
	-- 				}
	-- 			},
	--
	-- 			{
	-- 				name = "Tab",
	-- 				opts = {
	-- 					mode = "n",
	-- 					enter = { "<Leader>mt", "<C-m>t" },
	-- 					leave = { "<Esc>", "<C-c>", "q" },
	-- 				},
	-- 				mappings = {
	-- 					{
	-- 						lhs = "h",
	-- 						rhs = ":tabprevious<CR>",
	-- 					},
	-- 					{
	-- 						lhs = "l",
	-- 						rhs = ":tabnext<CR>",
	-- 					},
	-- 					-- {
	-- 					-- 	lhs = "x",
	-- 					-- 	rhs = ":tabclose<CR>",
	-- 					-- },
	-- 					{
	-- 						lhs = "t",
	-- 						rhs = ":tabnew<CR>",
	-- 					},
	-- 				},
	-- 			}
	-- 		}
	--
	-- 		create_submodes(submodes)
	-- 	end,
	-- 	keys = {
	-- 		{
	-- 			"<Leader>mr",
	-- 			desc = "Resize Submode",
	-- 		},
	-- 		{
	-- 			"<C-m>r",
	-- 			desc = "Resize Submode",
	-- 		},
	-- 		{
	-- 			"<Leader>mg",
	-- 			desc = "Git Submode",
	-- 		},
	-- 		{
	-- 			"<C-m>g",
	-- 			desc = "Git Submode",
	-- 		},
	-- 		{
	-- 			"<Leader>mt",
	-- 			desc = "Tab Submode",
	-- 		},
	-- 		{
	-- 			"<C-m>t",
	-- 			desc = "Tab Submode",
	-- 		},
	-- 	}
	-- },

	{
		"folke/flash.nvim",
		keys = {
			{
				"s",
				mode = { "n", "x", "o" },
				function()
					require("flash").jump()
				end,
				desc = "Flash",
			},
			{
				"S",
				mode = { "n", "x", "o" },
				function()
					require("flash").treesitter()
				end,
				desc = "Flash Treesitter",
			},
			{
				"r",
				mode = "o",
				function()
					require("flash").remote()
				end,
				desc = "Remote Flash",
			},
			{
				"R",
				mode = { "o", "x" },
				function()
					require("flash").treesitter_search()
				end,
				desc = "Treesitter Search",
			},
			{
				"<c-s>",
				mode = { "c" },
				function()
					require("flash").toggle()
				end,
				desc = "Toggle Flash Search",
			},
		},
	},

	-- lazy.nvim
	{
		"folke/noice.nvim",
		event = "VeryLazy",
		opts = {},
		dependencies = {
			-- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
			"MunifTanjim/nui.nvim",
		},
	},

	{
		"nanozuki/tabby.nvim",
		event = "VeryLazy",
		config = function()
			require("tabby.tabline").use_preset("tab_only", { lualine_theme = "onedark" })
		end,
		dependencies = "nvim-tree/nvim-web-devicons",
	},

	{
		"MagicDuck/grug-far.nvim",
		opts = {},
		cmd = { "GrugFar" },
	},

	-- {
	-- 	"olimorris/codecompanion.nvim",
	-- 	config = function()
	-- 		require("codecompanion").setup({
	-- 			adapters = {
	-- 				anthropic = require("codecompanion.adapters").use("anthropic", {
	-- 					env = {
	-- 						api_key = "ANTHROPIC_API_KEY",
	-- 					},
	-- 					schema = {
	-- 						model = {
	-- 							default = "claude-3-5-sonnet-20240620",
	-- 						},
	-- 					},
	-- 				}),
	-- 			},
	-- 			strategies = {
	-- 				chat = "anthropic",
	-- 				inline = "anthropic",
	-- 				tool = "anthropic",
	-- 			},
	-- 			keymaps = {
	-- 				["<C-g><C-g>"] = "keymaps.save",
	-- 			},
	-- 		})
	-- 	end,
	-- 	keys = function()
	-- 		return {
	-- 			{
	-- 				"<C-g><C-t>",
	-- 				function()
	-- 					vim.cmd([[CodeCompanionChat]])
	-- 				end,
	-- 				mode = { "n", "i", "v" },
	-- 			},
	-- 		}
	-- 	end,
	-- 	dependencies = {
	-- 		"nvim-lua/plenary.nvim",
	-- 		"nvim-treesitter/nvim-treesitter",
	-- 	},
	-- },

	-- automatic best guess indentation
	{
		"NMAC427/guess-indent.nvim",
		opts = {},
	},

	-- pretty, in terminal markdown renderer (alpha)
	{
		"OXY2DEV/markview.nvim",
		opts = {
			modes = { "n", "c" },
		},
	},

	"b0o/schemastore.nvim",

	{
		"jiaoshijie/undotree",
		dependencies = "nvim-lua/plenary.nvim",
		config = true,
		keys = { -- load the plugin only when using it's keybinding:
			{ "<leader>u", "<cmd>lua require('undotree').toggle()<cr>" },
		},
	},
}, {
	dev = {
		path = "~/.config/nvim/lua/custom-plugins/",
	},
})
