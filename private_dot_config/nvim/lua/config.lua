local opt = vim.opt
local wo = vim.wo

-- Show completion popup menu in command view
opt.wildmenu = true
-- Completion popup menu options
opt.completeopt = {
	"menuone", -- always show the menu
	"noinsert", -- do not insert match until manually selected
	"noselect", -- do not auto select match
}

-- https://vi.stackexchange.com/questions/10124/what-is-the-difference-between-filetype-plugin-indent-on-and-filetype-indent
vim.cmd([[filetype plugin indent on]])

-- opt.statusline = ' %f %r %m%=%y %{&fileencoding?&fileencoding:&encoding}[%{&fileformat}] %p%% %l:%c '

-- Don't pass messages to |ins-completion-menu|.
opt.shortmess:append("c")

-- Maintain undo history between sessions
opt.undofile = true
opt.undodir = os.getenv("HOME") .. [[/.vim/undodir/]]
opt.undolevels = 10000

-- How long before a swp file is written and a CursorHold event is triggered
-- Having longer updatetime (default is 4000 ms) leads to noticeable delays and poor user experience.
opt.updatetime = 100

-- Faster completion
opt.timeoutlen = 500

-- Always use system clipboard
opt.clipboard = "unnamed,unnamedplus"

-- Tab config
opt.tabstop = 4
opt.smarttab = true
opt.smartindent = true
opt.shiftwidth = 4

-- No noise please (no sound effects for errors)
opt.errorbells = false

opt.scrolloff = 8

-- Allow for width of numbers and arbitrary symbols in sign column
opt.signcolumn = "yes:1"

-- Case insensitive searching
opt.ignorecase = true
-- If a capital is included, make the search case sensitive
opt.smartcase = true

-- Let me backspace like I want
opt.backspace = { "indent", "eol", "start" }

-- Highlight the current line
opt.cursorline = true

-- ooo pretty colors o.o
opt.termguicolors = true

-- Set the height of the command bar to 0 line
opt.cmdheight = 0

-- Hybrid line numbers
-- absolute for current
-- relative for all others
wo.relativenumber = true
wo.nu = true

-- I save often enough
opt.swapfile = false

-- Spelling
opt.spelllang = "en_us"

opt.jumpoptions = "stack,view"
