-- Highlight yanked text on yank
vim.api.nvim_create_autocmd("TextYankPost", {
	callback = function()
		vim.highlight.on_yank()
	end,
	group = vim.api.nvim_create_augroup("YankHighlight", { clear = true }),
	pattern = "*",
})

-- Show cursor line only in active window
vim.api.nvim_create_autocmd({ "InsertLeave", "WinEnter" }, {
	callback = function()
		local ok, cl = pcall(vim.api.nvim_win_get_var, 0, "auto-cursorline")
		if ok and cl then
			vim.wo.cursorline = true
			vim.api.nvim_win_del_var(0, "auto-cursorline")
		end
	end,
})
vim.api.nvim_create_autocmd({ "InsertEnter", "WinLeave" }, {
	callback = function()
		local cl = vim.wo.cursorline
		if cl then
			vim.api.nvim_win_set_var(0, "auto-cursorline", cl)
			vim.wo.cursorline = false
		end
	end,
})

-- Go to last location when opening a buffer
vim.api.nvim_create_autocmd("BufReadPost", {
	callback = function()
		local mark = vim.api.nvim_buf_get_mark(0, '"')
		local lcount = vim.api.nvim_buf_line_count(0)

		if mark[1] > 0 and mark[1] <= lcount then
			pcall(vim.api.nvim_win_set_cursor, 0, mark)
		end
	end,
})

-- Close some filetypes with <q>
vim.api.nvim_create_autocmd("FileType", {
	pattern = {
		"qf",
		"help",
		"man",
		"notify",
		"lspinfo",
		"spectre_panel",
		"startuptime",
		"tsplayground",
		"PlenaryTestPopup",
	},
	callback = function(event)
		vim.bo[event.buf].buflisted = false
		vim.keymap.set("n", "q", "<cmd>close<cr>", { buffer = event.buf, silent = true })
	end,
})

-- Enable inlay hints
vim.api.nvim_create_autocmd({ "LspAttach", "InsertEnter", "InsertLeave" }, {
	callback = function(args)
		vim.lsp.inlay_hint.enable(args.event ~= "InsertEnter", { bufnr = args.buf })
	end,
})

local markdown = require("markdown")

-- Markdown buffer local mappings
vim.api.nvim_create_autocmd("FileType", {
	pattern = { "markdown" },
	callback = function(event)
		-- vim.keymap.set("n", "<Leader>mt", utils.toggle_checkbox, { buffer = event.buf, desc = "Toggle Checkbox" })
		vim.keymap.set({ "n", "i" }, "<C-t>", markdown.toggle_checkbox, { buffer = event.buf, desc = "Toggle Checkbox" })
		-- vim.keymap.set({ "n" }, "o", markdown.continue_bulleted_list, { buffer = event.buf })
		-- vim.keymap.set({ "i" }, "<cr>", markdown.continue_bulleted_list, { buffer = event.buf })
		-- vim.keymap.set({ "n" }, "O", function() markdown.continue_bulleted_list({forward = false }) end, { buffer = event.buf })
	end,
})

-- Markdown on save
vim.api.nvim_create_autocmd("BufWritePre", {
	pattern = { "*.md" },
	callback = function()
		print("Running markdown-toc")
		-- Get the current buffer
        local buf = vim.api.nvim_get_current_buf()

        -- Get the entire buffer contents
        local lines = vim.api.nvim_buf_get_lines(buf, 0, -1, false)

		-- Get the header locations
		local header_locations = {}
		for i, line in ipairs(lines) do
			if line:match("^#") then
				table.insert(header_locations, {i, 1})
			end
		end

		-- Get todo header using header locations
		-- match between 1 and 6 # characters + TODO
		local todo_header = "^%s*#+%s*TODO"
		local todo_header_location = nil
		for i, line in ipairs(header_locations) do
			if lines[line[1]]:match(todo_header) then
				todo_header_location = line
				break
			end
		end

		-- get the done header using header locations
		-- match between 1 and 6 # characters + DONE
		local done_header = "^%s*#+%s*DONE"
		local done_header_location = nil
		for i, line in ipairs(header_locations) do
			if lines[line[1]]:match(done_header) then
				done_header_location = line
				break
			end
		end

		-- move done items in the todo list to the done list
		-- local done_items = {}
		if todo_header_location then
			local start_line = todo_header_location[1]
			local end_line = done_header_location and done_header_location[1]

			for i = start_line + 1, end_line and end_line - 1 or #lines do
				local line = lines[i]

				if line:match("^%s*%- %[x%]") then
					-- add the line to the done items
					vim.fn.appendbufline(buf, #lines, lines[i])

					-- remove the line from the buffer
					vim.api.nvim_buf_set_lines(buf, i - 1, i, false, {})
				end
			end
		end

		-- if done_header_location then
		-- 	local start_line = done_header_location[1]
		-- 	local end_line = #lines
		--
		-- 	for i = start_line + 1, end_line do
		-- 		local line = lines[i]
		-- 		if line:match("^%s*%- %[ %]") then
		-- 			vim.fn.cursor(i, 0)
		-- 			-- get the line of the last item in the todo list
		-- 			local last_todo_item = vim.fn.search("^%s*%- %[ %]", "bnW")
		--
		-- 			-- add the line to the todo items
		-- 			vim.fn.appendbufline(buf, last_todo_item + 2, lines[i])
		--
		-- 			-- remove the line from the buffer
		-- 			vim.api.nvim_buf_set_lines(buf, i, i + 1, false, {})
		-- 		end
		-- 	end
		-- end
	end,
})
