#################################################################################
# Keep at top of file
#################################################################################

'~/.config/nushell/extras.nu' | path exists; source ~/.config/nushell/extras.nu


#################################################################################
# Aliases
#################################################################################

alias v = nvim
alias la = ls -a
alias s = zellij
alias mitm = mitmproxy
alias fuck = with-env {TF_ALIAS: "fuck", PYTHONIOENCODING: "utf-8"} {
    thefuck (history | last 1 | get command.0)
}
alias godot = /Applications/Godot_mono.app/Contents/MacOS/Godot
alias lg = lazygit
alias ld = lazydocker

export def yeet [message?] {
	git add .;

	if $message == null {
		$message == "updates"
	}

	git commit -m $message;

	git push;
}

export def gco [] {
	git rev-parse --is-inside-work-tree;

	let chosen_branch = (
		git branch
		| fzf
		| str trim --left --char '*'
		| str trim
	);

	let is_empty = $chosen_branch | is-empty;

	if $is_empty {
		print 'No branch chosen';

		return;
	}

	git checkout $chosen_branch;
}

export def fi [search_term] {
	let chosen_package = (
		flox search $search_term --all
		| fzf
		| split row " "
		| get 0
		| str trim
	)

	let is_empty = $chosen_package | is-empty;

	if $is_empty {
		print 'No package selected';

		return;
	}

	flox install $chosen_package;
}

export def auth [authCommand: string] {
	nu -c $authCommand

	pAuth
}


#################################################################################
# Keep at bottom of file
#################################################################################

source ~/.config/nushell/default-config.nu
source ~/.config/nushell/mise.nu
source ~/.oh-my-posh.nu
source ~/.zoxide.nu
source ~/.local/share/atuin/init.nu
